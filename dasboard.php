<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>AdminLTE | Data Tables</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->

            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="login.php">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>Masuk</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
            <aside class="col-md-12">
               
                <section class="content">
                            <div class="box box-primary">
                                <div class="box-body pad table-responsive">
                                    <h3 align="center">STANDAR SARANA DAN PRASARANA INVENTARIS SMK&nbsp; <br> &nbsp;</h3>
									<p align="center"><img src="img/tiga.jpg" width="300" height="200"></p>
									<br/>
									<p align="justify">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
Setiap satuan pendidikan wajib memiliki sarana yang meliputi perabot, peralatan pendidikan, media pendidikan, buku dan sumber belajar lainnya, bahan habis pakai, 
serta perlengkapan lain yang diperlukan untuk menunjang proses pembelajaran yang teratur dan berkelanjutan.
Setiap satuan pendidikan wajib memiliki prasarana yang meliputi lahan, ruang kelas, ruang pimpinan satuan pendidikan, ruang pendidik, ruang tata usaha, ruang perpustakaan, 
ruang laboratorium, ruang bengkel kerja, ruang unit produksi, ruang kantin, instalasi daya dan jasa, tempat berolahraga, tempat beribadah, tempat bermain, tempat berkreasi,
 dan ruang/tempat lain yang diperlukan untuk menunjang proses pembelajaran yang teratur dan berkelanjutan.

<p>Berikut ini, Peraturan Menteri Pendidikan Nasional Republik Indonesia yang berkaitan dengan Standar Sarana dan Prasarana. </p>
	<ul>
	<li> Peraturan Menteri Pendidikan Nasional Republik Indonesia No 24 Tahun 2007 tentang Standar Sarana dan Prasarana untuk Sekolah Dasar/Madrasah Ibtidaiyah (SD/MI), 
	Sekolah Menengah Pertama/Madrasah Tsanawiyah (SMP/MTs), dan Sekolah Menengah Atas/Madrasah Aliyah (SMA/MA). </li>
	<li>Peraturan Menteri Pendidikan Nasional Republik Indonesia No 40 Tahun 2008 tentang Standar Sarana Prasarana untuk Sekolah Menengah Kejuruan (SMK) 
	dan Madrasah Aliyah Kejuruan (MAK).</li>
	<li>Peraturan Menteri Pendidikan Nasional Republik Indonesia Nomor 33 Tahun 2008 tentang Standar Sarana Prasarana untuk Sekolah  Luar Biasa.</li>
	</ul>
									
									</p>
									<br>

                                </div><!-- /.box -->
                            </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>       
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
    </body>
</html>