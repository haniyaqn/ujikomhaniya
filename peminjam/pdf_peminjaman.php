<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>
	<style>
	table {border-collapse:collapse; table-layout:fixed;width: 630px:}
	table td {word-wrap:break-word;width: 11%}
	</style>
</head>
					<body>
							<h1 style="text-align:center;">Laporan Data Peminjaman</h1>
								<table align="center" border="1" width="100%">
								<tr>
												<th>No</th>
												<th>Tanggal pinjam </th>
                                                <th>Status peminjaman</th>
												<th>Nama pegawai</th>
								</tr>
		<?php
		include "koneksi.php";
		$no=1;
		$select=mysqli_query($conn,"SELECT * FROM peminjaman i left join jenis j on j.id_jenis=i.id_jenis
														left join ruang r on r.id_ruang=i.id_ruang
														left join petugas p on p.id_petugas=i.id_petugas");
		while($data=mysqli_fetch_array($select))
		{
		?>
		<tr align="center">
			<td><?php echo $no++; ?></td>
					<td><?php echo $data ['tanggal_pinjam' ]; ?></td>
					<td><?php echo $data ['status_peminjaman' ]; ?></td>
					<td><?php echo $data ['nama_pegawai' ]; ?></td>
		</tr>
		<?php
		}
		?>
</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('L','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data peminjaman.pdf', 'D');
?>