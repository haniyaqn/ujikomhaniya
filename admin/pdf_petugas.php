<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>
	<style>
	table {border-collapse:collapse; table-layout:fixed;width: 630px:}
	table td {word-wrap:break-word;width: 11%}
	</style>
</head>
<body>
<h1 style="text-align:center;">Laporan Data Petugas</h1>
<table align="center" border="1" width="100%">
<tr>
	<th align="center">No</th>
	<th align="center">Id petugas</th>
    <th align="center">Username</th>
    <th align="center">Password</th>
	<th align="center">Nama petugas</th>
	<th align="center">Id level</th>
</tr>
		<?php
		include "koneksi.php";
		$no=1;
		$select=mysqli_query($conn,"SELECT * FROM petugas");
		while($data=mysqli_fetch_array($select))
		{
		?>
		<tr align="center">
			<td><?php echo $no++; ?></td>
			<td><?php echo $data['id_petugas']; ?></td>
			<td><?php echo $data['username']; ?></td>
			<td><?php echo $data['password']; ?></td>
			<td><?php echo $data['nama_petugas']; ?></td>
			<td><?php echo $data['id_level']; ?></td>
		</tr>
		<?php
		}
		?>
</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('L','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Petugas.pdf', 'D');
?>