<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>AdminLTE | Data Tables</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                INVENTARIS
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->

                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="treeview">
                             <a href="beranda.php">
                                <i class="fa fa-home"></i> <span>Beranda</span>
                            </a>
                        </li>
						<li>
                            <a href="inventaris.php">
                                <i class="fa fa-user"></i> <span>Inventaris</span>
                            </a>
                        </li>
						<li>
                            <a href="peminjaman.php">
                                <i class="fa fa-user"></i> <span>Peminjaman</span>
                            </a>
                        </li>
                        <li>
                            <a href="pengembalian.php">
                                <i class="fa fa-calendar"></i> <span>Pengembalian</span>
                            </a>
                        </li>
						
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-edit"></i> <span>Lainnya</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
							<ul class="treeview-menu">
                                <li><a href="jenis.php"><i class="fa fa-angle-double-right"></i>Jenis</a></li>
                                <li><a href="ruang.php"><i class="fa fa-angle-double-right"></i> Ruang </a></li>		
								<li><a href="petugas.php"><i class="fa fa-angle-double-right"></i> Petugas </a></li>				
								<li><a href="pegawai.php"><i class="fa fa-angle-double-right"></i> Pegawai </a></li>				
								<li><a href="detail_pinjam.php"><i class="fa fa-angle-double-right"></i> Detail pinjam </a></li>				
                            </ul>
							<li>
                            <a href="generat_laporan.php">
                                <i class="fa fa-calendar"></i> <span>Laporan</span>
                            </a>
                        </li>
                            <ul class="treeview-menu">
                                <li><a href="id_jenis.php"><i class="fa fa-angle-double-right"></i>Jenis</a></li>
                                <li><a href="id_ruang.php"><i class="fa fa-angle-double-right"></i> Ruang </a></li>		
								<li><a href="id_petugas.php"><i class="fa fa-angle-double-right"></i> Petugas </a></li>				
								<li><a href="pegawai.php"><i class="fa fa-angle-double-right"></i> Pegawai </a></li>				
								<li><a href="detail_pinjam.php"><i class="fa fa-angle-double-right"></i> Detail pinjam </a></li>					
                            </ul>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->


                <!-- Main content -->
                <section class="content">


                    <!-- /.row -->

                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">                            
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Tambah Detail Barang</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                                           <div class="box">							
                               <div class="box-body table-responsive">
						<form action="simpan_detail.php" method="post" role="form">	
<?php
include "koneksi.php";
 $auto=mysqli_query($conn,"select * from peminjaman order by id_peminjaman desc limit 1");
$no=mysqli_fetch_array($auto);
$angka=$no['id_peminjaman']+1;
?>							
							
							<div class="form-group row">
								<label class="col-sm-2 control-label">Nama Barang</label>
									<div class="col-sm-6">
											<select name="id_inventaris[]" class="form-control" >
											<option>---Pilih---</option>
											<?php
											$conn = mysqli_connect("localhost","root","","ujikom");
											$result = mysqli_query($conn, "SELECT id_inventaris,nama FROM inventaris");
											while($row = mysqli_fetch_assoc($result))
											{
												echo "<option>$row[id_inventaris].$row[nama]</option>";
											}
											?>
											</select>
									</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Jumlah</label>
									<div class="col-sm-6">
										<input name="jml[]" type="number" class="form-control">
										<input name="id_detail_pinjam[]" type="hidden" value="<?php echo $angka;?>" class="form-control">
									</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Nama pegawai</label>
									<div class="col-sm-6">
											<select name="id_pegawai[]" class="form-control" >
											<option>---Pilih---</option>
											<?php
											$conn = mysqli_connect("localhost","root","","ujikom");
											$result = mysqli_query($conn, "SELECT id_pegawai,nama_pegawai FROM pegawai");
											while($row = mysqli_fetch_assoc($result))
											{
												echo "<option>$row[id_pegawai].$row[nama_pegawai]</option>";
											}
											?>
											</select>
									</div>
							</div>
							<hr>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Nama Barang</label>
									<div class="col-sm-6">
											<select name="id_inventaris[]" class="form-control" >
											<option>---Pilih---</option>
											<?php
											$conn = mysqli_connect("localhost","root","","ujikom");
											$result = mysqli_query($conn, "SELECT id_inventaris,nama FROM inventaris");
											while($row = mysqli_fetch_assoc($result))
											{
												echo "<option>$row[id_inventaris].$row[nama]</option>";
											}
											?>
											</select>
									</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Jumlah</label>
									<div class="col-sm-6">
										<input name="jml[]" type="number" class="form-control">
										<input name="id_detail_pinjam[]" type="hidden" value="<?php echo $angka;?>" class="form-control">
									</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Nama pegawai</label>
									<div class="col-sm-6">
											<select name="id_pegawai[]" class="form-control" >
											<option>---Pilih---</option>
											<?php
											$conn = mysqli_connect("localhost","root","","ujikom");
											$result = mysqli_query($conn, "SELECT id_pegawai,nama_pegawai FROM pegawai");
											while($row = mysqli_fetch_assoc($result))
											{
												echo "<option>$row[id_pegawai].$row[nama_pegawai]</option>";
											}
											?>
											</select>
									</div>
							</div>
							<div class="form-group">
							
							<button type="submit" class="btn btn-info">Save</button>
							<button type="reset" class="btn btn-info">Cancel</button>
							</div>
						</form>
                                
									
                                </div><!-- /.box-body -->                               
                            </div><!-- /.box -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content --> 
                        <!-- right col (We are only adding the ID to make the widgets sortable)-->
 
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>       
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
    </body>
</html>