
<!DOCTYPE html>


<html>
    <head>
        <meta charset="UTF-8">
        <title>AdminLTE | Data Tables</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
       <header class="header">
            <a href="../../index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                INVENTARIS
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->

                        <!-- Notifications: style can be found in dropdown.less -->
                      
                        <!-- Tasks: style can be found in dropdown.less -->
                      
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>Masuk<i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                               
                               
                                
                                <li class="user-footer">
                                
                                    <div class="pull-right">
                                        <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">

            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
						<section class="sidebar">
                    <!-- Sidebar user panel -->

                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                   
						</section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->


                <!-- Main content -->
                <section class="content">


                    <!-- /.row -->

                    <!-- Main row -->
 <div>
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <i class="fa fa-edit"></i>
                                    <h3 class="box-title"></h3>
                                </div>
                                <div class="box-body pad table-responsive">
                                    <p align="center"><h3>STANDAR SARANA DAN PRASARANA</h3></p>
									<p><h5>
Setiap satuan pendidikan wajib memiliki sarana yang meliputi perabot, peralatan pendidikan, media pendidikan, buku dan sumber belajar lainnya, bahan habis pakai, serta perlengkapan lain yang diperlukan untuk menunjang proses pembelajaran yang teratur dan berkelanjutan.

Setiap satuan pendidikan wajib memiliki prasarana yang meliputi lahan, ruang kelas, ruang pimpinan satuan pendidikan, ruang pendidik, ruang tata usaha, ruang perpustakaan, ruang laboratorium, ruang bengkel kerja, ruang unit produksi, ruang kantin, instalasi daya dan jasa, tempat berolahraga, tempat beribadah, tempat bermain, tempat berkreasi, dan ruang/tempat lain yang diperlukan untuk menunjang proses pembelajaran yang teratur dan berkelanjutan.
</p>
<p>
Berikut ini, Peraturan Menteri Pendidikan Nasional Republik Indonesia yang berkaitan dengan Standar Sarana dan Prasarana.</p>
<p>

1. Peraturan Menteri Pendidikan Nasional Republik Indonesia No 24 Tahun 2007 tentang Standar Sarana dan Prasarana untuk Sekolah Dasar/Madrasah Ibtidaiyah (SD/MI), Sekolah Menengah Pertama/Madrasah Tsanawiyah (SMP/MTs), dan Sekolah Menengah Atas/Madrasah Aliyah (SMA/MA).
<br/>
<br/>2. Peraturan Menteri Pendidikan Nasional Republik Indonesia No 40 Tahun 2008 tentang Standar Sarana Prasarana untuk Sekolah Menengah Kejuruan (SMK) dan Madrasah Aliyah Kejuruan (MAK).
<br/>
<br/>3. Peraturan Menteri Pendidikan Nasional Republik Indonesia Nomor 33 Tahun 2008 tentang Standar Sarana Prasarana untuk Sekolah  Luar Biasa.
									</h5>
									</p>
									<br><br><br><br>
									<br>
									<br>
									<br>
									<br>
									<br>

                                </div><!-- /.box -->
                            </div>
                        </div><!-- /.col -->
                    </div><!-- ./row -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>       
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
    </body>
</html>