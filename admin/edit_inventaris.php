<!DOCTYPE html>
<?php
include "koneksi.php";
$id_inventaris=$_GET['id_inventaris'];
$select=mysqli_query($conn, "select * from inventaris where id_inventaris='$id_inventaris'");
$data=mysqli_fetch_array($select);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>AdminLTE | Data Tables</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
           <header class="header">
                        <a href="../../index.html" class="logo" style="background: #367fa9;">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <font color="white">INVENTARIS</font>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation" style="background: #367fa9;">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->

                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                        <ul class="sidebar-menu">
					<li>
                            <a href="beranda.php">
                                <i class="fa fa-home"></i> <span>Beranda</span>
                            </a>
                        </li>
						<li>
                            <a href="inventaris.php">
                                <i class="fa fa-user"></i> <span>Inventaris</span>
                            </a>
                        </li>
						<li>
                            <a href="peminjaman.php">
                                <i class="fa fa-user"></i> <span>Peminjaman</span>
                            </a>
                        </li>
                        <li>
                            <a href="pengembalian.php">
                                <i class="fa fa-calendar"></i> <span>Pengembalian</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-edit"></i> <span>Lainnya</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="nama.php"><i class="fa fa-angle-double-right"></i>Nama</a></li>
                                <li><a href="kondisi.php"><i class="fa fa-angle-double-right"></i> Kondisi </a></li>		
								<li><a href="ktrngn_inventaris.php"><i class="fa fa-angle-double-right"></i> keterangan </a></li>				
								<li><a href="jumlah.php"><i class="fa fa-angle-double-right"></i> jumlah </a></li>				
								<li><a href="id_jenis.php"><i class="fa fa-angle-double-right"></i> jenis </a></li>				
								<li><a href="tanggal_register.php"><i class="fa fa-angle-double-right"></i> Tanggal register </a></li>				
								<li><a href="id_ruang.php"><i class="fa fa-angle-double-right"></i> Ruang </a></li>				
								<li><a href="kode_inventaris.php"><i class="fa fa-angle-double-right"></i> kode inventaris </a></li>				
								<li><a href="id_petugas.php"><i class="fa fa-angle-double-right"></i> petugas </a></li>				
                            </ul>
							 <li>
                            <a href="generat_laporan.php">
                                <i class="fa fa-calendar"></i> <span>Laporan</span>
                            </a>
                        </li>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
				
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->


                <!-- Main content -->
                <section class="content">


                    <!-- /.row -->

                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">                            
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Edit inventaris</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
								<?php
								$id=$_GET['id_inventaris'];
								$select=mysqli_query($conn,"select  a
								left join jenis b on b.id_jenis=a.id_jenis
								left join ruang c on c.id_ruang=a.id_ruang
								left join petugas d on d.id_petugas=a.id_petugas
								where id_inventaris='".$id."'
								");
								$select_jenis=mysqli_query($conn, "select `id_jenis`,`nama_jenis` from `jenis`");
								$select_ruang=mysqli_query($conn, "select `id_ruang`,`nama_ruang` from `ruang`");
								$select_petugas=mysqli_query($conn, "select `id_petugas`,`nama_petugas` from `petugas`");
								?>
                                 <form action="update_inventaris.php?id_inventaris=<?php echo $id_inventaris; ?>" method="post" role="form">
                                    <div class="box-body">
									<div class="form-group">
                                            <label for="disabled">No </label>
                                            <input name="id_inventaris" type="text" class="form-control" value="<?php echo $data['id_inventaris'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Nama </label>
                                            <input name="nama" type="text" class="form-control" value="<?php echo $data['nama'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Kondisi </label>
                                            <input name="kondisi" type="text" class="form-control" value="<?php echo $data['kondisi'];?>">
                                        </div>
										<div class="form-group">
                                            <label for="exampleInputPassword1">Keterangan</label>
                                            <input name="ktrngn_inventaris" type="text" class="form-control" value="<?php echo $data['ktrngn_inventaris'];?>">
                                        </div>
										 <div class="form-group">
                                            <label for="exampleInputPassword1">Jumlah</label>
                                            <input name="jumlah" type="number" class="form-control" value="<?php echo $data['jumlah'];?>">
                                        </div> 
										<div class="form-group">
                                            <label for="exampleInputPassword1">Jenis</label>
                                            <input name="id_jenis" type="text" class="form-control" value="<?php echo $data['id_jenis'];?>">
                                        </div> 
										<div class="form-group">
                                            <label for="exampleInputPassword1">Tanggal register</label>
                                            <input name="tanggal_register" type="text" class="form-control active" value="<?php echo $data['tanggal_register'];?>" readonly>
                                        </div>
										<div class="form-group">
										<label>Nama Ruang</label>
												<div class="controls">
													<select class="form-control span8" name="id_ruang">
											<?php while ($data_ruang=mysqli_fetch_array($select_ruang)){ ?>
												<option value="<?php echo $data_ruang['id_ruang']; ?>"
													<?php if($data['id_ruang']==$data_ruang['id_ruang']){ echo "selected"; }?>>
													<?php echo $data_ruang['nama_ruang']; ?>
														</option>
											<?php } ?>
													</select>
													</div>
										</div>										
										<div class="form-group">
                                            <label for="exampleInputPassword1">Kode inventaris</label>
                                            <input name="kode_inventaris" type="text" class="form-control" value="<?php echo $data['kode_inventaris'];?>">
                                        </div>
										<div class="form-group">
										<label>Nama petugas</label>
												<div class="controls">
													<select class="form-control span8" name="id_petugas">
											<?php while ($data_petugas=mysqli_fetch_array($select_petugas)){ ?>
												<option value="<?php echo $data_petugas['id_petugas']; ?>"
													<?php if($data['id_petugas']==$data_petugas['id_petugas']){ echo "selected"; }?>>
													<?php echo $data_petugas['nama_petugas']; ?>
														</option>
											<?php } ?>
													</select>
													</div>
										</div>
													
										
                                        
                                        </div>

                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content --> 
                        <!-- right col (We are only adding the ID to make the widgets sortable)-->
 
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>       
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
    </body>
</html>