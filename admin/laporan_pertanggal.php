<?php
include ('cek.php');
error_reporting(0);
?>
<!DOCTYPE html>


<html>
    <head>
        <meta charset="UTF-8">
        <title>AdminLTE | Data Tables</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
       <header class="header">
           <a href="../../index.html" class="logo" style="background: #367fa9;">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <font color="white">INVENTARIS</font>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation" style="background: #367fa9;">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->

                        <!-- Notifications: style can be found in dropdown.less -->
                      
                        <!-- Tasks: style can be found in dropdown.less -->
                      
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $_SESSION['username'];?><i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                               
                               
                                
                                <li class="user-footer">
                                
                                    <div class="pull-right">
                                        <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">

            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
						<?php
						if($_SESSION['id_level']==1){
               
			   echo'<section class="sidebar">
                    <!-- Sidebar user panel -->

                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
					
						<li>
                            <a href="inventaris.php">
                                <i class="fa fa-user"></i> <span>Inventaris</span>
                            </a>
                        </li>
						<li>
                            <a href="peminjaman.php">
                                <i class="fa fa-user"></i> <span>Peminjaman</span>
                            </a>
                        </li>
                        <li>
                            <a href="pengembalian.php">
                                <i class="fa fa-calendar"></i> <span>Pengembalian</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-edit"></i> <span>Lainnya</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="jenis.php"><i class="fa fa-angle-double-right"></i>Jenis</a></li>
                                <li><a href="ruang.php"><i class="fa fa-angle-double-right"></i> Ruang </a></li>		
								<li><a href="petugas.php"><i class="fa fa-angle-double-right"></i> Petugas </a></li>				
								<li><a href="pegawai.php"><i class="fa fa-angle-double-right"></i> Pegawai </a></li>				
								<li><a href="detail_pinjam.php"><i class="fa fa-angle-double-right"></i> Detail pinjam </a></li>									
                            </ul>
							 <li>
                            <a href="laporan.php">
                                <i class="fa fa-calendar"></i> <span>Laporan</span>
                            </a>
                        </li>
                        </li>
                    </ul>
						</section>';
						}elseif($_SESSION['id_level']==2){
               
			   echo'<section class="sidebar">
                    <!-- Sidebar user panel -->

                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
					    <li>
                            <a href="beranda.php">
                                <i class="fa fa-home"></i> <span>Beranda</span>
                            </a>
                        </li>
						
						<li>
                            <a href="peminjaman.php">
                                <i class="fa fa-user"></i> <span>Peminjaman</span>
                            </a>
                        </li>
                        <li>
                            <a href="pengembalian.php">
                                <i class="fa fa-calendar"></i> <span>Pengembalian</span>
                            </a>
                        </li>
						 
                       
                    </ul>
						</section>';
						}elseif($_SESSION['id_level']==3){
               
			   echo'<section class="sidebar">
                    <!-- Sidebar user panel -->

                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
					<li>
                            <a href="beranda.php">
                                <i class="fa fa-home"></i> <span>Beranda</span>
                            </a>
                        </li>
						
						<li>
                            <a href="peminjaman.php">
                                <i class="fa fa-user"></i> <span>Peminjaman</span>
                            </a>
                        </li>
                       
                    </ul>
						</section>';
						}
						?>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->


                <!-- Main content -->
                <section class="content">

					<div class="panel-body">
						<div class="col-lg-5">
					<label>Pilih Tanggal</label>
											<form method="get">
											<select name="tanggal" class="form-control m-bot15">
												<?php
												include "koneksi.php";
												//display values in combobox/dropdown
												$result = mysqli_query($conn,"SELECT day(tanggal_pinjam) as tanggal FROM peminjaman GROUP BY DAY(tanggal_pinjam)");
												while($row = mysqli_fetch_assoc($result))
												{
												echo "<option value='$row[tanggal]'>$row[tanggal]</option>";
												} 
												?>
											</select>
													<br/>
												<button type="submit" class="btn btn-outline btn-primary">Tampilkan</button>
												<br>
									<div class="col-md-8"></div>
											</form>
										</div>
								 </div>

						 <?php if(isset($_GET['tanggal'])) { ?>
						 
						 <div class="box-content">
								<table class="table table-striped table-bordered bootstrap-datatable datatable">
											<thead> 
														 <tr>
															<th>No.</th>
															<th>ID Peminjaman</th>
															<th>Nama Barang</th>
															<th>Jumlah Pinjam</th>
															<th>Tanggal Pinjam</th>
															<th>Tanggal Kembali</th>
															<th>Nama Pegawai</th>
												
															</tr> 
											</thead>   
											<tbody>
											<?php
									$no=1;
										$select=mysqli_query($conn,"select * from peminjaman a left join pegawai b on b.id_pegawai=a.id_pegawai
																left join detail_pinjam d on a.id_peminjaman=d.id_detail_pinjam
																left join inventaris i on i.id_inventaris=d.id_inventaris WHERE DAY(tanggal_pinjam) = '$_GET[tanggal]'");
									
												while($data=mysqli_fetch_array($select))
												{
												?>
												  <tr>
													<td><?php echo $no++; ?></td>
													<td><?php echo $data['id_peminjaman']; ?></td>
													<td><?php echo $data['nama']; ?></td>
													<td><?php echo $data['jumlah']; ?></td>
													<td><?php echo $data['tanggal_pinjam']; ?></td>
													<td><?php echo $data['tanggal_kembali']; ?></td>
													<td><?php echo $data['nama_pegawai']; ?></td>
													</tr>
													</tbody>
													<?php
							}
							?>
								</table>  
								 <a href="laporan_pertanggal.php"><button type="submit" class="btn btn-primary">Per Tanggal </button></a>
								 <a href="laporan_perbulan.php"><button type="submit" class="btn btn-primary">Per Bulan </button></a>
								 <a href="laporan_pertahun.php"><button type="submit" class="btn btn-primary">Per Tahun </button></a>
								 <a href="./pdf_pertanggal.php"><button type="submit" class="btn btn-primary">PDF Per Tanggal </button></a>
							</div>
					
							</div>
							<?php
							}
							?>
<div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detail Data Barang</h4>
                </div>
                <div class="modal-body">
                    <div class="fetched-data"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                </div>
            </div>
        </div>
    </div>
						</div>
                    </div>

                </section><!-- /.content --> 
                        <!-- right col (We are only adding the ID to make the widgets sortable)-->
 
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
		
		<script type="text/javascript">
    $(document).ready(function(){
        $('#myModal').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id_inventaris');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                type : 'post',
                url : 'detail_barang.php',
                data :  'rowid='+ rowid,
                success : function(data){
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });
  </script>
		
        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>       
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
    </body>
</html>