<?php
include ('cek.php');
error_reporting(0);
?>
<!DOCTYPE html>


<html>
    <head>
        <meta charset="UTF-8">
        <title>AdminLTE | Data Tables</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
       <header class="header">
            <a href="../../index.html" class="logo" style="background: #367fa9;">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <font color="white">INVENTARIS</font>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation" style="background: #367fa9;">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->

                        <!-- Notifications: style can be found in dropdown.less -->
                      
                        <!-- Tasks: style can be found in dropdown.less -->
                      
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $_SESSION['username'];?><i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                               
                               
                                
                                <li class="user-footer">
                                
                                    <div class="pull-right">
                                        <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">

            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
						<?php
						if($_SESSION['id_level']==1){
               
			   echo'<section class="sidebar">
                    <!-- Sidebar user panel -->

                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
					
						<li>
                            <a href="inventaris.php">
                                <i class="fa fa-user"></i> <span>Inventaris</span>
                            </a>
                        </li>
						<li>
                            <a href="peminjaman.php">
                                <i class="fa fa-user"></i> <span>Peminjaman</span>
                            </a>
                        </li>
                        <li>
                            <a href="pengembalian.php">
                                <i class="fa fa-calendar"></i> <span>Pengembalian</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-edit"></i> <span>Lainnya</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="jenis.php"><i class="fa fa-angle-double-right"></i>Jenis</a></li>
                                <li><a href="ruang.php"><i class="fa fa-angle-double-right"></i> Ruang </a></li>		
								<li><a href="petugas.php"><i class="fa fa-angle-double-right"></i> Petugas </a></li>				
								<li><a href="pegawai.php"><i class="fa fa-angle-double-right"></i> Pegawai </a></li>
								<li><a href="detail_pinjam.php"><i class="fa fa-angle-double-right"></i> Detail pinjam </a></li>								
                            </ul>
							 <li>
                            <a href="laporan.php">
                                <i class="fa fa-calendar"></i> <span>Laporan</span>
                            </a>
                        </li>
                        </li>
                    </ul>
						</section>';
						}elseif($_SESSION['id_level']==2){
               
			   echo'<section class="sidebar">
                    <!-- Sidebar user panel -->

                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
					    
						
						<li>
                            <a href="peminjaman.php">
                                <i class="fa fa-user"></i> <span>Peminjaman</span>
                            </a>
                        </li>
                        <li>
                            <a href="pengembalian.php">
                                <i class="fa fa-calendar"></i> <span>Pengembalian</span>
                            </a>
                        </li>
						 
                       
                    </ul>
						</section>';
						}elseif($_SESSION['id_level']==3){
               
			   echo'<section class="sidebar">
                    <!-- Sidebar user panel -->

                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
					<li>
                            <a href="beranda.php">
                                <i class="fa fa-home"></i> <span>Beranda</span>
                            </a>
                        </li>
						
						<li>
                            <a href="peminjaman.php">
                                <i class="fa fa-user"></i> <span>Peminjaman</span>
                            </a>
                        </li>
                       
                    </ul>
						</section>';
						}
						?>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->


                <!-- Main content -->
                <section class="content">


                    <!-- /.row -->

                    <!-- Main row -->
                    <div class="row">
   <div class="col-lg-12">
                <section class="">
                    <div class="">
                        <div class="position-center">
							<div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Input Pengembalian</h3>	
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    
									<div class="row">
                                    
									<section class="col-xs-4"><center>
										<label> Pilih Id Peminjaman</label>
											<div class="box-body no-padding">
												<div class="container">
													<form align="center" class="col-lg-3" method="POST">
														<select name="id_peminjaman" class="form-control col-lg-3">
								<?php
								include "koneksi.php";
								//display values in combobox/dropdown
								$result = mysqli_query($conn,"SELECT id_peminjaman from peminjaman where status_peminjaman='Pinjam'");
								while($row = mysqli_fetch_assoc($result))
								{
								echo "<option value='$row[id_peminjaman]'>$row[id_peminjaman]</option>";
								} 
								?>
									</select>
									<br/><br/>
								<button type="submit" name="pilih" class="btn btn-primary"> Kembalikan </button>
														<br/>
													</form>
													
												</div>
											</div>
									</center></section>
									
									<section class="col-xs-12">
											<div class="box-body no-padding">
												<div class="container">
									<?php
						if(isset($_POST['pilih'])){?>
                               <form action="proses_pengembalian.php" method="post" role="form">
							                                               <?php
										include "koneksi.php";
										$id_peminjaman=$_POST['id_peminjaman'];
										$select=mysqli_query($conn,"select * from peminjaman p left join detail_pinjam d on d.id_detail_pinjam=p.id_peminjaman
																								left join inventaris e on d.id_inventaris=e.id_inventaris
																								left join pegawai i on p.id_pegawai=i.id_pegawai
																			where id_peminjaman='$id_peminjaman'");
										$selectt=mysqli_query($conn,"select * from peminjaman p left join detail_pinjam d on d.id_detail_pinjam=p.id_peminjaman
																								where id_peminjaman='$id_peminjaman'");
										
										while($data=mysqli_fetch_array($select))
										while($dataa=mysqli_fetch_array($selectt)){
										?>
										
										<br/>
										<div class="form-group">
											<label>Id Peminjaman</label>
                                        <div class="input-group col-md-6">
                                            <input name="id_peminjaman" value="<?php echo $data['id_peminjaman'];?>" type="hidden" class="form-control" required Readonly />
                                            <input name="id_detail_pinjam" value="<?php echo $data['id_detail_pinjam'];?>" type="hidden" class="form-control" required Readonly />
                                            <input name="id_inventaris" value="<?php echo $data['id_inventaris'];?>" type="text" class="form-control" required Readonly />
                                        </div>
										</div>
										
										<div class="form-group">
											<label>Inventaris</label>
                                        <div class="input-group col-md-6">
                                            <input name="id_inventaris" value="<?php echo $data['id_inventaris'];?>.<?php echo $data['nama'];?>" type="text" class="form-control" required Readonly />
                                        </div>
										</div>
										
										<div class="form-group">
											<label>Nama Pegawai</label>
                                        <div class="input-group col-md-6">
                                            <input name="id_pegawai" value="<?php echo $data['nama_pegawai'];?>" type="text" class="form-control" required Readonly />
                                            <input name="tanggal_kembali" type="hidden" class="form-control" required Readonly />
                                        </div>
										</div>
										
										<div class="form-group">
											<label>Jumlah Pinjam</label>
                                        <div class="input-group col-md-6">
                                            <input name="jumlah_pinjam" value="<?php echo $data['jumlah_pinjam'];?>" type="text" class="form-control" required Readonly />
                                        </div>
										</div>
										

										
										<div class="control-group">
											<div class="box-footer">
												<button type="submit" class="btn btn-outline btn-primary">Submit</button>
												<button type="reset" class="btn btn-outline btn-danger">Reset</button>
											</div>
										</div>
										<?php } ?>
                                 </form>
								<?php } ?>
								
												</div><!-- /.row - inside box -->
											</div><br/>
									</section>
									
								</div>
									                              
								</div>
							
							</div><br/>
						
						
                    <div class="row">
                        <div class="col-xs-12">                            
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Tabel Pengembalian</h3> 
									
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
								
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama barang</th>
												<th>Tanggal Pinjam</th>
												<th>Tanggal Kembali</th>
                                                <th>Nama pegawai</th>
                                                <th>Jumlah</th>
												<th>Status peminjaman</th>
                                            </tr>
                                        </thead>
									<tbody>
        <?php
		include "koneksi.php";
		$no=1;
		$sql= "select * from peminjaman p left join detail_pinjam d on d.id_detail_pinjam=p.id_peminjaman
											left join inventaris e on d.id_inventaris=e.id_inventaris
											left join pegawai i on p.id_pegawai=i.id_pegawai where status_peminjaman='Kembali'";
		$select=mysqli_query($conn, $sql);
		while($data=mysqli_fetch_array($select))
		{
		?>
                                 
                                            <tr class="warning">
                                            <td><?php echo $no++; ?></td>
											<td><?php echo $data ['nama']; ?></td>
											<td><?php echo $data ['tanggal_pinjam']; ?></td>
											<td><?php echo $data ['tanggal_kembali']; ?></td>
											<td><?php echo $data ['nama_pegawai']; ?></td>
											<td><?php echo $data ['jumlah_pinjam']; ?></td>
											<td><?php echo $data ['status_peminjaman']; ?></td>
											
											
											
                                        </tr>
										<?php
										}
										?>
                                    </tbody>

                                    </table>
									
                                </div><!-- /.box-body -->                               
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content --> 
                        <!-- right col (We are only adding the ID to make the widgets sortable)-->
 
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>       
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
    </body>
</html>