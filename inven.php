<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>
	<style>
	table {border-collapse:collapse; table-layout:fixed;width: 630px:}
	table td {word-wrap:break-word;width: 11%}
	</style>
</head>
<body>
<h1 style="text-align:center;">Laporan Data Inventaris</h1>
<table align="center" border="1" width="100%">
<tr>
	<th align="center">No</th>
	<th align="center">Kode Inventaris</th>
	<th align="center">Nama Barang</th>
	<th align="center">Kondisi</th>
	<th align="center">Keterangan</th>
	<th align="center">Jumlah</th>
	<th align="center">Tanggal Register</th>
</tr>
		<?php
		include "koneksi.php";
		$no=1;
		$select=mysqli_query($conn,"SELECT * FROM inventaris ");
		while($data=mysqli_fetch_array($select))
		{
		?>
		<tr align="center">
			<td width="25" align="center"><?php echo $no++; ?></td>
			<td><?php echo $data['kode_inventaris']; ?></td>
			<td><?php echo $data['nama']; ?></td>
			<td><?php echo $data['kondisi']; ?></td>
			<td><?php echo $data['ktrngn_inventaris']; ?></td>
			<td><?php echo $data['jumlah']; ?></td>
			<td><?php echo $data['tanggal_register']; ?></td>
		</tr>
		<?php
		}
		?>
</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('L','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Inventaris.pdf', 'D');
?>